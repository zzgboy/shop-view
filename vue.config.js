const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  //关闭语法检测
  lintOnSave: false,
  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          javascriptEnabled: true
        }
      }
    },
  },
  //开启代理服务器
  devServer: {
    client: {
      overlay: false
    },
    proxy: {
      '/shop': {
        // target: 'http://192.168.200.129:9201',
        target: 'http://127.0.0.1:9201',
        pathRewrite: { '^/shop': '' },
        ws: true, //用于支持websocket
        changeOrigin: false
      }
    }
  }
})
