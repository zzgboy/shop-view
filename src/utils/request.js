// 二次封装axios请求
import axios from 'axios'

const http = axios.create({
    //通用请求的地址前缀
    // baseURL:'http://192.168.200.129:8080/',
    baseURL:'http://127.0.0.1:8080/',
    //超时时间
    timeout:10000
})

// 添加请求拦截器
http.interceptors.request.use(function (config) {
  // 1.从缓存中获取到token,这里的Authorization时登录时你给用户设置token的键值
  let authorization = window.sessionStorage.getItem("token");
  let tokenHead = window.sessionStorage.getItem("tokenHead");
  // 2.如果token不为null，那么设置到请求头中，此处哪怕为null，我们也不进行处理，因为后台会进行拦截
  if (authorization) {
  //后台给登录用户设置的token的键时什么，headers['''']里的键也应该保持一致
    config.headers['Authorization'] = tokenHead+authorization;
  }
  // 3.放行
  return config;
  }, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  });

// 添加响应拦截器
// http.interceptors.response.use(function (response) {
//     // 对响应数据做点什么
//     return response;
//   }, function (error) {
//     // 对响应错误做点什么
//     return Promise.reject(error);
//   });

export default http