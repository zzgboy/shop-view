//是否手机号码或者固话，只能由字母、数字组成，1-9位
export const validatePassword= (rule, value, callback) => {
    const reg = /^(?=.*\d)(?=.*[A-z])[\da-zA-Z]{1,9}$/;;
    if (value == '' || value == undefined || value == null) {
        callback();
    } else {
        if ((!reg.test(value)) && value != '') {
            callback(new Error('请输入正确的密码'));
        } else {
            callback();
        }
    }
}

//是否手机号码
export const validatePhone = (rule, value, callback) => {
    const reg = /^[1][3-9][0-9]{9}$/;
    if (value == '' || value == undefined || value == null) {
        callback();
    } else {
        if ((!reg.test(value)) && value != '') {
            callback(new Error('请输入正确的电话号码'));
        } else {
            callback();
        }
    }
}

//是否邮箱
export const validateEMail = (rule, value, callback) => {
    const reg = /^([a-zA-Z0-9]+[-_\.]?)+@[a-zA-Z0-9]+\.[a-z]+$/;
    if (value == '' || value == undefined || value == null) {
        callback();
    } else {
        if (!reg.test(value)) {
            callback(new Error('请输入正确的邮箱'));
        } else {
            callback();
        }
    }
}