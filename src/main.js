import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import VueRouter from 'vue-router'
//引入路由器
import router from './router'
//导入全局样式表
import './assets/css/global.css'
import '../src/plugins/element/element'
//导入字体图标
import './assets/font/iconfont.css'
import SIdentify from './components/SIdentify'
//导入axios
// import axios from 'axios' 

// Vue.prototype.$http = axios
// //配置请求的根路径
// axios.defaults.baseURL = 'http://192.168.200.129:9201/'
Vue.config.productionTip = false
Vue.use(ElementUI);
Vue.use(VueRouter)

Vue.component(SIdentify.name,SIdentify)

new Vue({
  render: h => h(App),
  router:router
}).$mount('#app')
