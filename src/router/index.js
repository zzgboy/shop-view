import VueRouter from 'vue-router'
import Login from '../components/Login'
import Home from '../components/Home'
import Register from '../components/Register'
import Welcome from '../components/Welcome'
import Users from '../components/user/Users'
import Roles from '../components/role/Roles'
import Authoritys from '../components/authority/Authoritys'

const router = new VueRouter({
    routes: [
        //该路由的意思是如果用户访问的是http://localhost:8080/#/则自动重定向到http://localhost:8080/#/login
        {
            path: '/',
            redirect: '/login'
        },
        {
            path: '/login',
            component: Login
        },
        {
            path: '/home',
            component: Home,
            redirect: '/welcome',
            children: [
                {
                    path: '/welcome',
                    component: Welcome
                },
                {
                    path: '/users',
                    component: Users
                },
                {
                    path: '/roles',
                    component: Roles
                },
                {
                    path: '/authoritys',
                    component: Authoritys
                },
            ]
        },
        {
            path: '/register',
            component: Register
        }
    ]
})

//挂载路由导航守卫
router.beforeEach((to, from, next) => {
    //如果用户直接访问登录页面则放行
    if (to.path == '/login' || to.path == '/register') return next()
    //从sessionStorage获取token
    const tokenStr = window.sessionStorage.getItem('token')
    //如果tokenStr为空，那么就跳转到登录页面
    if (!tokenStr) return next('/login')
    //如果不为空则放行
    next()
})

export default router