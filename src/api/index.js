import http from '../utils/request'

//定义一个登录请求的接口
export const userLogin = (username,password) => {
    //返回一个promise对象
    return http({
        url: 'shop/auth/oauth/token',
        method: 'post',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
            username,
            password,
            grant_type: 'password',
            client_id: 'client-app',
            client_secret: '123456',
        }
    })
}

//定义一个注册请求的接口
export const userRegister = (registerForm) => {
    //返回一个promise对象
    return http({
        url: 'shop/api/admin/register',
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        data: {
            account: registerForm.account,
            password:registerForm.password,
            nickName: registerForm.nickName,
            phone: registerForm.phone,
            email: registerForm.email,
            sex: registerForm.sex,
        }
    })
}

//定义一个获取所有菜单请求的接口
export const queryMenuList = () => {
    //返回一个promise对象
    return http({
        url: 'shop/api/menu/queryMenuList',
        method: 'post'
    })
}

//定义一个获取所有用户请求的接口
export const queryUserList = (queryInfo) => {
    //返回一个promise对象
    return http({
        url: 'shop/api/admin/queryUserList',
        method: 'get',
        headers: {
            'Content-Type': 'application/json'
        },
        params: {
            account: queryInfo.query,
            pageNum:queryInfo.pageNum,
            pageSize: queryInfo.pageSize
        }
    })
}

//定义一个获取所有用户请求的接口
export const updateUser = (admin) => {
    //返回一个promise对象
    return http({
        url: 'shop/api/admin/updateUser',
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        data: admin
    })
}

//定义一个获取所有用户请求的接口
export const getMessage = () => {
    //返回一个promise对象
    return http({
        url: 'shop/api/hello',
        method: 'get',
        headers: {
            'Content-Type': 'application/json'
        }
    })
}

//添加用户
export const addAdmin = (addForm) => {
    //返回一个promise对象
    return http({
        url: 'shop/api/admin/addUser',
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        data: {
            account: addForm.account,
            password:addForm.password,
            email: addForm.email,
            phone: addForm.phone
        }
    })
}

//删除用户
export const rmAdmin = (id) => {
    //返回一个promise对象
    return http({
        url: 'shop/api/admin/rmUser',
        method: 'get',
        params:{
            id:id
        }
    })
}

//根据用户名获取用户信息
export const getUserDemo = (str) => {
    //返回一个promise对象
    return http({
        url: 'shop/api/admin/queryUser',
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        data:{
            account:str
        }
    })
}

//获取角色列表
export const queryRoleList = () => {
    return http({
        url: 'shop/api/role/queryRoleList',
        method: 'get',
        params:{
            
        }
    })
}

export const modifyUserDemo = (userForm) => {
    return http({
        url: 'shop/api/admin/updateUser',
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        data:userForm
    })
}
//上传图片
export const uploadImage = (param) => {
    return http({
        url: 'shop/minio/minio/user/uploadImage',
        method: 'post',
        headers: {
            'Content-Type': 'Multipart/form-data'
        },
        data:param
    })
}